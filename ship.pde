class Ship {
  //Define class variables
  PVector location;
  PVector velocity = new PVector(0, 0);
  PVector force;
  boolean isAlive;
  boolean isThrusting = false;
  boolean isTurning = false;
  final int safeTimeBetweenDeaths = 1000;
  final int r = 15; // Size of the ship
  final int maxEnergy = 100;
  final int numFrames = 1;
  int energy;
  int lives = 3;
  int timeOfLastDeath;
  int score;
  int playerMinEnergy = 50;
  float heading = 0;
  float rotation;

  /**
    Function: Ship()
    Description: Constructor for the Ship class, Spawns the ship in the
                  middle of the screen and sets its status to alive.
    Parameters:  None
    Returns: None
  */
  Ship() {
    isAlive = true;
    timeOfLastDeath = 0;
    score = 0;
    energy = 100;
    location = new PVector(width/2, height/2);
  }

  /**
    Function: update()
    Description: Updates the ships location and checks if the ship is
                  thrusting to turn the ship.
    Parameters: None
    Returns: Void
  */
  void update() {
    isAlive = isAlive();
    if (isThrusting) {
      thrust();
    }
    location.add(velocity);
    velocity.mult(0.99);
    checkEdges();
    turn();

    //Recharge ray gun energy when not full.
    if(frameCount % numFrames == 0 && energy < maxEnergy) {
      energy++;
    }
  }

  /**
    Function: draw()
    Description: Draws the ship at its location using the triangle
                  function from processing.
    Parameters: None
    Returns: Void
  */
  void draw() {
    float offset = PI / 2;
    push();
    translate(location.x, location.y);
    rotate(heading + offset);
    //If the ship is thrusting draw thrusters.
    if (isThrusting) {
      fill(255, 0, 0);
      stroke(255, 0, 0);
      triangle(-r, r, -r/3, r, -2*r/3, 1.5*r);
      triangle(-r/3, r, r/3, r, 0, 1.5*r);
      triangle(r/3, r, r, r, 2*r/3, 1.5*r);
    }
    //If the ship is turning draw bigger thrusters on left or right.
    if (isTurning) {
      fill(255, 0, 0);
      stroke(255, 0, 0);
      if (rotation > 0) {
        triangle(-r, r, -r/3, r, -2*r/3, 2*r);
      } else {
        triangle(r/3, r, r, r, 2*r/3, 2*r);
      }
    }
    //Draw the ship last.
    fill(0);
    stroke(255);
    triangle(-r, r, r, r, 0, -r);
    pop();
  }

  /**
    Function: checkEdges()
    Description: Checks if the ship is off the screen and changes its
                  location to the other side of the screen if it is.
    Parameters: None
    Returns: Void
  */
  void checkEdges() {
    if (location.x > (width + r)) {
      location.x = -r;
    } else if (location.x < -r) {
      location.x = width + r;
    }
    
    if (location.y > (height + r)) {
      location.y = -r;
    } else if (location.y < -r) {
      location.y = height + r;
    }
  }

  /**
    Function: setRotation()
    Description: Sets the rotation variable.
    Parameters: float(x): The number to set rotation to.
    Returns: Void
  */
  void setRotation(float x) {
    rotation = x;
  }

  /**
    Function: turning()
    Description: Sets isTurning to the provided boolean.
    Parameters: boolean(b): The status to set the isTurning boolean to.
    Returns: Void
  */
  void turning(boolean b) {
    isTurning = b;
  }

  /**
    Function: turn()
    Description: Updates the heading using the rotation variable.
                  Heading is limited to between -2 * PI and 2 * PI.
    Parameters: None
    Returns: Void
  */
  void turn() {
    heading += rotation;
    if ((heading > (2 * PI)) || (heading < (-2 * PI))) {
      heading = 0;
    }
  }

  /**
    Function: thrusting()
    Description: Sets isThrusting to the provided boolean.
    Parameters: boolean(b): The status to set the isThrusting boolean to.
    Returns: Void
  */
  void thrusting(boolean b) {
    isThrusting = b;
  }

  /**
    Function: thrust()
    Description: Thrusts the ship forward using a Vector.
    Parameters: None
    Returns: Void
  */
  void thrust() {
    force = PVector.fromAngle(heading);
    force.mult(0.05);
    velocity.add(force);
  }

  /**
    Function: collide()
    Description: Checks if the ship is colliding with the provided asteroid.
    Parameters: Asteroid(a): The Asteroid to check.
    Returns: Boolean
  */
  boolean collide(Asteroid a) {
    float d = dist(location.x, location.y, a.getLoc().x, a.getLoc().y);
    if (d < a.getMaxSize()) {
      return true;
    }
    return false;
  }

  /**
    Function: collide()
    Description: Checks if the shot has collided with the alien and 
                  returns true if it has.
    Parameters: Alien(a): the alien to check if its been hit.
    Returns: Boolean
  */
  boolean collide(Alien a) {
    /*
      Referenced from the following site;
      https://happycoding.io/tutorials/processing/collision-detection
    */
    if ((location.x > (a.getLoc().x - a.getWidth()/2)) && 
          (location.x < (a.getLoc().x + a.getWidth()/2))) {
      if ((location.y > (a.getLoc().y - a.getHeight()/2)) && 
          (location.y < (a.getLoc().y + a.getHeight()/2))) {
        return true;
      }
    }
    return false;
  }

  /**
    Function: getLives()
    Description: Returns the number of lives the player has.
    Parameters: None
    Returns: int
  */
  int getLives() {
    return lives;
  }

  /**
    Function: hit()
    Description: Removes a life from the player depending on the time expired 
                 so that the player doesn't die right away.
    Parameters: None
    Returns: Void
  */
  void hit() {
    if (lives > 0 && 
        ((liveGameTimer - timeOfLastDeath) > safeTimeBetweenDeaths)) {
      timeOfLastDeath = liveGameTimer;
      lives--;
    }
  }

  /**
    Function: isAbleToFire()
    Description: Checks if the player has enough energy to fire.
    Parameters: None
    Returns: boolean
  */
  boolean isAbleToFire() {
    if (energy > playerMinEnergy) {
      return true;
    } else {
      return false;
    }
  }

  /**
    Function: subEnergy()
    Description: Subtracts the input value from the ships energy.
    Parameters: int(x): Energy to subtract from players gun to.
    Returns: boolean
  */
  void subEnergy(int x) {
    energy -= x;
  }

  /**
    Function: isAlive()
    Description: Returns the status of the players health.
    Parameters: None
    Returns: Boolean
  */
  boolean isAlive() {
    return (lives != 0);
  }

  /**
    Function: addScore()
    Description: Adds a value to the existing score.
    Parameters: int(x): Score to add.
    Returns: Void
  */
  void addScore(int x) {
    score+= x;
  }

  /**
    Function: subtractScore()
    Description: Subtracts a value from the existing score.
    Parameters: int(x): Score to subtract.
    Returns: Void
  */
  void subtractScore(int x) {
    score-= x;
  }

  /**
    Function: getScore()
    Description: Returns the players score.
    Parameters: None
    Returns: Int
  */
  int getScore() {
    return score;
  }

  /**
    Function: getLoc()
    Description: Returns the location of the ship.
    Parameters: None
    Returns: PVector
  */
  PVector getLoc() {
    return location;
  }

  /**
    Function: getSize()
    Description: Returns the Size of the ship.
    Parameters: None
    Returns: int
  */
  int getSize() {
    return r;
  }

  /**
    Function: getHeading()
    Description: Returns the heading of the ship.
    Parameters: None
    Returns: float
  */
  float getHeading() {
    return heading;
  }
}
